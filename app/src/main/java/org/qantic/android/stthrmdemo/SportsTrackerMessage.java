package org.qantic.android.stthrmdemo;

/**
 * Created by ntrp on 8/17/14.
 */
public class SportsTrackerMessage {

    private int batteryLevel;
    private int heartRate;
    private int[] beatsPairDelta;

    public SportsTrackerMessage() {
        beatsPairDelta = new int[6];
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int[] getBeatsPairDelta() {
        return beatsPairDelta;
    }

    public void setBeatsPairDelta(int[] beatsPairDelta) {
        this.beatsPairDelta = beatsPairDelta;
    }
}
