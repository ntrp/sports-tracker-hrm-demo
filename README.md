# README #

This project is a simple demo about reading heart rate values from a sports tracker HRM v1 belt (the old model). The base code for bluetooth interaction was borrowed from this project [Bluetooth Viewer](https://github.com/janosgyerik/bluetoothviewer).

After the connection has been established a thread is started and it begins to parse the data using the protocol analysis provided in the bottom table.
```
#!java
public void run() {
    Log.i(TAG, "BEGIN mConnectedThread");

    BufferedReader reader = new BufferedReader(new InputStreamReader(mmInStream));

    byte[] buffer = new byte[1024];
    byte[] accumulator = new byte[1024];
    int bytes;
    int accumulatedBytes = 0;
    int packetBytes = 0;
    int sequenceNumber = 0;
    boolean receiving = false;

    while (!stop) {
        try {
            bytes = mmInStream.read(buffer);

            String dataBin = "";
            for (int i = 0; i < bytes; i++) {
                dataBin += String.format("%8s", Integer.toBinaryString(buffer[i] & 0xFF)).replace(' ', '0') + " ";
            }
            Log.d(TAG, "read: " + bytes + " bytes. " + "\nBIN : " + dataBin);

            if (buffer[0] == 0xFFFFFFFA) {
                receiving = true;
            }
            if (bytes > 0 && bytes < 33 && receiving) {
                System.arraycopy(buffer, 0, accumulator, accumulatedBytes, bytes);
                accumulatedBytes = buffer[0] == 0xFFFFFFFA ? bytes : accumulatedBytes + bytes;
                if (accumulatedBytes > 1) {
                    packetBytes = accumulator[1] >> 2;
                    sequenceNumber = accumulator[1] & 0x03;
                }
                if (accumulatedBytes >= packetBytes) {
                    SportsTrackerMessage stm = new SportsTrackerMessage();
                    stm.setBatteryLevel(accumulator[4] & 0x3F);
                    stm.setHeartRate(accumulator[5] & 0xFF);
                    int[] pairsDelta = stm.getBeatsPairDelta();
                    pairsDelta[0] = getByteToNibble(accumulator[6], accumulator[7]);
                    pairsDelta[1] = getNibbleToByte(accumulator[7], accumulator[8]);
                    pairsDelta[2] = getByteToNibble(accumulator[9], accumulator[10]);
                    pairsDelta[3] = getNibbleToByte(accumulator[10], accumulator[11]);
                    pairsDelta[4] = getByteToNibble(accumulator[12], accumulator[13]);
                    pairsDelta[5] = getNibbleToByte(accumulator[13], accumulator[14]);
                    sendSTMessage(stm);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "disconnected", e);
            connectionLost();
            break;
        }
    }
}

private int getByteToNibble (byte b, byte n) {
    return (b << 4) + ((n & 0xF0) >> 4);
}

private int getNibbleToByte (byte n, byte b) {
    return ((n & 0x0F) << 8) + b;
}
```

To get the heart rate value we get the 6th byte of the accumulator buffer and convert it to an unsigned int:

```
#!java
accumulator[5] & 0xFF

```

Here there is the Sports Tracker HRM packet analysis gently provided by the user TheSeven:

| Byte | Structure | Description                                                                                                                                                                                                        |
|:----:|:---------:|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   0  |  11111010 | Packet header magic byte (used for synchronization)                                                                                                                                                                |
|   1  |  llllllcc | llllll: Packet length in bytes (always 17), cc: Sequence number (0 => 1 => 2 => 3 => 0 => ...)                                                                                                                     |
|   2  |    byte   | Binary inverse of byte 1                                                                                                                                                                                           |
|   3  |  ???????? | x0000001 values seem to have special meanings, always 0x81?                                                                                                                                                        |
|   4  |  ??bbbbbb | ??: Always 0?, bbbbbb: Battery level                                                                                                                                                                               |
|   5  |    byte   | Averaged heart rate (beats per minute)                                                                                                                                                                             |
|   6  |    byte   | High byte of 12-bit time between first pair of beats (RRI) in milliseconds                                                                                                                                         |
|   7  | xxxx yyyy | xxxx: Low nibble of 12-bit time between first pair of beats (RRI) in milliseconds, yyyy: High nibble of 12-bit time between second pair of beats (RRI) in milliseconds (zero if not present)                       |
|   8  |    byte   | Low byte of 12-bit time between second pair of beats (RRI) in milliseconds (zero if not present)                                                                                                                   |
|   9  |    byte   | High byte of 12-bit time between third pair of beats (RRI) in milliseconds (zero if not present)                                                                                                                   |
|  10  | xxxx yyyy | xxxx: Low nibble of 12-bit time between third pair of beats (RRI) in milliseconds (zero if not present), yyyy: High nibble of 12-bit time between fourth pair of beats (RRI) in milliseconds (zero if not present) |
|  11  |    byte   | Low byte of 12-bit time between fourth pair of beats (RRI) in milliseconds (zero if not present)                                                                                                                   |
|  12  |    byte   | High byte of 12-bit time between fourth pair of beats (RRI) in milliseconds (zero if not present)                                                                                                                  |
|  13  | xxxx yyyy | xxxx: Low nibble of 12-bit time between fifth pair of beats (RRI) in milliseconds (zero if not present), yyyy: High nibble of 12-bit time between sixth pair of beats (RRI) in milliseconds (zero if not present)  |
|  14  |    byte   | Low byte of 12-bit time between sixth pair of beats (RRI) in milliseconds (zero if not present)                                                                                                                    |
|  15  |    byte   | Checksum byte one (unknown algorithm)                                                                                                                                                                              |
|  16  |    byte   | Checksum byte two (unknown algorithm)                                                                                                                                                                              |